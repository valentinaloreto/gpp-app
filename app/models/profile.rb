class Profile < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :role, optional: true

  scope :clients, -> {where(role_id: 3)}
  scope :executives, -> {where(role_id: 2)}
  scope :admins, -> {where(role_id: 1)}

  accepts_nested_attributes_for :user
end
