class Project < ApplicationRecord
  belongs_to :tag, optional: true
  belongs_to :account
  has_many :archives, inverse_of: :project, dependent: :destroy
  has_many :comments, dependent: :destroy

  enum status: { in_progress: 0, delivered: 1, approved: 2, rejected: 3 }
  enum kind: { default: 0, multichoice: 1, grilla: 2 }

  accepts_nested_attributes_for :tag, reject_if: :all_blank
  accepts_nested_attributes_for :archives, reject_if: :all_blank, allow_destroy: true

  def all_archives_approved?
    !archives.map {|archive| archive.approved }.include?(false)
  end

  def all_my_attachments
    archives.map {|archive| archive}
  end
end
