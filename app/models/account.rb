class Account < ApplicationRecord
  belongs_to :client, optional: true,  foreign_key: :client_id, class_name: 'User'
  belongs_to :executive, optional: true, foreign_key: :executive_id, class_name: 'User'

  has_many :projects, dependent: :destroy

  accepts_nested_attributes_for :client

  def projects_by_tag(tag_name)
    projects.joins(:tag).where("tags.name = ?", tag_name)
  end

  def all_my_tags
    projects.map {|p| if p.tag then [p.tag.name, p.tag.id] end }.uniq.compact
  end

  def all_my_tags_uniq_name
    p = projects.map {|p| if p.tag then [p.tag.name, p.tag.id] end }.compact
    p.uniq { |i| i[0] }
  end

end
