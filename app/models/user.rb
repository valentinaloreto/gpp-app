class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :profile, dependent: :delete
  has_one :account, foreign_key: :client_id, class_name: 'Account'
  has_many :accounts, foreign_key: :executive_id, class_name: 'Account'
  has_many :comments

  accepts_nested_attributes_for :profile
end
