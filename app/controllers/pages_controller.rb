class PagesController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :verify_session

  def home
    if current_user
      if current_user.profile.role.kind == "client"
        if params[:tag]
          @tags = current_user.account.all_my_tags
          @projects = current_user.account.projects_by_tag(params[:tag])
        else
          @projects = current_user.account.projects
          @tags = current_user.account.all_my_tags
        end
      end
    end
  end

  private
  
  def verify_session
    redirect_to new_user_session_path unless user_signed_in?
  end

  def set_date_now
    @today = DateTime.now.to_date
  end
end
