class AccountsController < ApplicationController
  before_action :is_admin?
  before_action :redirect_if_not_admin, except: :show
  before_action :set_account, only: [:show, :edit, :destroy, :update]
  before_action :redirect_if_not_admin_or_associated_executive, only: :show
  before_action :set_executives, only: [:new, :edit, :create]

  def index
    @accounts = Account.all
  end

  def show
    #abort params.inspect
    @account = Account.find(params[:id])
    if @account.projects.any?
      if params[:tag]
        @projects = @account.projects_by_tag(params[:tag])
        # @account.projects.joins(:tag).where(:tags => { :name => params[:format] })
      else
        @projects = @account.projects
      end
      @tags = @account.projects.map { |p| if p.tag then p.tag.name end }.uniq.compact
    end
  end

  def new
    @account = Account.new
    @client = @account.build_client
    @profile = @client.build_profile
    @profile[:role_id] = "3"
  end

  def create
    #abort account_params.inspect
    @account = Account.new(account_params)
    if @account.save
      redirect_to accounts_path, notice: 'Account was successfully created.'
    else
      render :new
    end
  end

  def edit
    @account = Account.find(params[:id])
    @client = @account.client
    @profile = @client.profile
  end

  def update
    @account = Account.find(params[:id])
    if @account.update(account_params)
      redirect_to account_path(@account), notice: 'Your info was successfully updated.'
    else
      render :edit
    end

  end

  def destroy
    @client = @account.client
    if @account.destroy
      @client.destroy
      redirect_to accounts_path, notice: "Info was successfully destroyed"
    end
  end

  private

  def set_account
    @account = Account.find(params[:id])
  end

  def account_params
    params.require(:account).permit(
      :name, :executive_id, client_attributes: [:id, :email, :password, profile_attributes: [:id, :firstname, :lastname, :role_id] ]
    )
  end

  def set_executives
    @executives = Profile.executives.map { |p| ["#{p.firstname} #{p.lastname}" , p.user.id] }
  end

  def is_admin?
    @is_admin = current_user.profile.role_id == 1
  end

  def redirect_if_not_admin
    unless @is_admin then redirect_to root_path end
  end

  def redirect_if_not_admin_or_associated_executive
    @is_executive_in_charge = @account.executive_id == current_user.id
    unless @is_executive_in_charge || @is_admin then redirect_to root_path end
  end

end
