class ArchivesController < ApplicationController
  def destroy
    @archive = Archive.find(params[:id])
    @project = @archive.project
    if @archive.destroy
      redirect_to edit_project_path(@project), notice: "Archive was successfully destroyed!!!!!!!!!!!1111!!1111ONEONEONE"
    end
  end
end
