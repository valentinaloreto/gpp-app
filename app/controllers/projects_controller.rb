class ProjectsController < ApplicationController

  before_action :is_admin?
  before_action :set_account, only: [:new, :create]
  before_action :set_project, except: [:create, :new, :update_status, :update_approval_archive]
  # before_action :set_blobs, only: [:show, :edit]
  before_action :is_client?, only: [:show]
  before_action :is_delivered?, only: [:show]
  before_action :is_default?, only: [:show]
  before_action :redirect_if_not_admin_or_associated_executive, except: [:show, :update_status, :update_approval_archive ]
  before_action :redirect_if_not_admin_or_related, only: :show

  def show
    @comentarios = @project.comments
    @comments = @project.comments.build
  end

  def new
    @project = @account.projects.build
    @tag = @project.build_tag
    @archives = @project.archives.build
    @tags = @account.all_my_tags_uniq_name
  end

  def create
    @project = @account.projects.build(project_params)

    if @project.tag then @project.tag.name.upcase! end

    if @project.save
      if @project.tag then @project.tag.name.upcase! end
      @project.save
      redirect_to project_path(@project), notice: 'Project was successfully created.'
    else
      @tags = @project.account.all_my_tags
      render :new
    end
  end

  def edit
    @account = @project.account
    @tag = Tag.new
    @archivos = @project.all_my_attachments
    @archives = @project.archives.build
    @tags = @account.all_my_tags_uniq_name
  end

  def update
    if @project.update(project_params)
      if @project.tag then @project.tag.name.upcase! end
      if  @project.status == "delivered" then UserMailer.project_delivered(@project.account.client,@project).deliver_now end
      if  (@project.status == "approved" || @project.status == "rejected") 
        UserMailer.project_evaluated(@project.account.executive,@project).deliver_now 
      end
      @project.save
      redirect_to project_path(@project), notice: 'Your info was successfully updated.'
    else
      @account = @project.account
      @tag = Tag.new
      @archivos = @project.all_my_attachments
      @archives = @project.archives.build
      @tags = @project.account.all_my_tags
      render :edit
    end
  end

  def destroy
    @account = @project.account
    @tag = @project.tag
    if @project.destroy
      if @tag.projects.empty? then @tag.destroy end
      redirect_to account_path(@account), notice: "Info was successfully destroyed"
    end
  end

  def update_status
    @project = Project.find(params[:id])
    case params[:status]
    when "aprobado"
      @project.update({status: "approved"})
      redirect_to project_path(@project), notice: "Info was successfully destroyed"
    when "rechazado"
      @project.update({status: "rejected"})
      redirect_to project_path(@project), notice: "Info was successfully destroyed"
    when "delivered"
      @project.update({status: "delivered"})
      redirect_to edit_project_path(@project), notice: "Info was successfully destroyed"
    when "in_progress"
      @project.update({status: "in_progress"})
      redirect_to edit_project_path(@project), notice: "Info was successfully destroyed"
    end
  end

  def update_approval_archive
    @project = Project.find(params[:project_id])
    @archive = Archive.find(params[:id])
    case params[:status]
    when "aprobado"
      @archive.update({approved: true})
      @project.update({status: "approved"})
      redirect_to project_path(@project), notice: "Info was successfully destroyed"
    when "rechazado"
      @archive.update({approved: false})
      redirect_to project_path(@project), notice: "Info was successfully destroyed"
    end
  end

  private

  def set_account
    @account = Account.find(params[:account_id])
  end

  def set_project
    @project = Project.find(params[:id])
  end

  def set_blobs
    @archives = Project.find(params[:id]).archives
    @images = @archives.documents
    # Archive.all[3].document.attachment.blob
  end

  def project_params
    params.require(:project).permit(:name, :description, :account_id,
      :responsible, :kind, :status, :start_date, :deadline, :next_steps,
      :tag_id, tag_attributes: [:name], archives_attributes: [:id, :title, :description, :document])
  end

  def is_admin?
    @is_admin = current_user.profile.role_id == 1
  end

  def is_client?
    @is_client = @project.account.client_id == current_user.id
  end

  def is_delivered?
    @is_delivered = @project.status == "delivered"
  end

  def is_default?
    @is_default = @project.kind == "default"
  end

  def redirect_if_not_admin_or_associated_executive
    if @project
      @is_executive_in_charge = @project.account.executive_id == current_user.id
    else
      @is_executive_in_charge = @account.executive_id == current_user.id
    end
    unless @is_executive_in_charge || @is_admin then redirect_to root_path end
  end

  def redirect_if_not_admin_or_related
    @is_executive_in_charge = @project.account.executive_id == current_user.id
    @is_client = @project.account.client_id == current_user.id
    unless @is_executive_in_charge || @is_admin || @is_client then redirect_to root_path end
  end

  def controller_action
    status = params[:status]
  end

end
