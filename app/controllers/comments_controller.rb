class CommentsController < ApplicationController
  before_action :set_project, only: :create
  def create
    @comment = @project.comments.build(comment_params)
    @comment[:user_id] = current_user.id
    if @comment.save
      # @comment.save
      if current_user.profile.role_id == 3
        @receiver = @comment.project.account.executive
      else
        @receiver = @comment.project.account.client 
      end
      UserMailer.new_comment(@receiver, @project).deliver_now
      redirect_to project_path(@project), notice: 'Comment was successfully created.'
    else
      @tags = @project.account.all_my_tags
      redirect_to project_path(@project)
    end
  end

  def destroy

    @comment = Comment.find(params[:id])
    @project = @comment.project
    if @comment.destroy
      redirect_to project_path(@project), notice: "Comment was successfully destroyed"
    end
  end

  private

  def set_project
    @project = Project.find(params[:project_id])
  end

  def comment_params
    params.require(:comment).permit(:content, :user_id)
  end
end
