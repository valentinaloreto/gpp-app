class ExecutivesController < ApplicationController
  before_action :redirect_if_not_admin
  before_action :set_executive, only: [:show, :destroy, :update]

  def index
    @executives = Profile.executives.map {|profile| profile.user }
  end

  def show
  end

  def new
    @user = User.new
    @profile = @user.build_profile
    @profile[:role_id] = 2
  end

  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.new_user(@user,@user.profile).deliver_now
      redirect_to executives_path, notice: 'Executive was successfully created.'
    else
      render :new
    end

  end

  def edit
    @user = User.find(params[:id])
    @profile = @user.profile
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to executives_path, notice: 'Your info was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @executive.accounts.each {|a| a.update({executive_id: nil}) }
    if @executive.destroy
      redirect_to executives_path, notice: "Info was successfully destroyed"
    end
  end

  private

  def set_executive
    @executive = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, profile_attributes: [:firstname, :lastname, :role_id])
  end

  def redirect_if_not_admin
    if current_user.profile.role_id != 1 then redirect_to root_path end
  end
end
