class ApplicationMailer < ActionMailer::Base
  default from: 'exampleGGG@gmail.com'
  layout 'mailer'
end
