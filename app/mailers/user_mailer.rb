class UserMailer < ApplicationMailer
    # descomentar en config/development config.action_mailer.delivery_method
    # cambiar en mailers/ApplicationMailer el default
    # en config application.yml colocar el correo y la clave
    # para previsualizar:
    # http://localhost:3000/rails/mailers/user_mailer/project_delivered.html
    # http://localhost:3000/rails/mailers/user_mailer/new_user.html
    # http://localhost:3000/rails/mailers/user_mailer/project_evaluated.html
    # http://localhost:3000/rails/mailers/user_mailer/new_comment.html

  def new_user(user,profile)
    @profile = profile
    mail(to: user.email, subject: 'Se ha creado una cuenta para usted en GPP-APP!')
  end

  def new_comment(receiver, project)
    @profile = receiver.profile
    @project = project
    mail(to: receiver.email, subject: 'Tienes un nuevo comentario por revisar!')
  end

  def project_evaluated(user,project)
    @profile = user.profile
    @project = project
    mail(to: user.email, subject: 'Tiene un proyecto evaluado!')
  end


  def project_delivered(user,project)
    @profile = user.profile
    @project = project
    mail(to: user.email, subject: 'Tiene un proyecto por evaluar!')
  end
end
