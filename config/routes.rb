Rails.application.routes.draw do
  root to: 'pages#home'
  devise_for :users, :path_prefix => "my", skip: :registration
  resources :users, only: [:edit, :update, :destroy]
  resources :executives, except: :show
  resources :archives, only: [:destroy]
  resources :accounts, shallow: true do
    resources :projects, except: [:index] do
      resources :comments , only: [:create, :destroy]
    end
  end
  get '/projectstatus/:id/:status', to: 'projects#update_status', as: 'update_project_status'
  get '/archivestatus/:id/:status', to: 'projects#update_approval_archive', as: 'update_archive_status'
end
