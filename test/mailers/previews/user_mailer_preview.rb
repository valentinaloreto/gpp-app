# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  def new_user
      UserMailer.with(user: User.last).new_user(User.last, User.last.profile)
  end

  def project_delivered
      UserMailer.with(user: User.last).project_delivered(Project.last.account.client, Project.last)
  end

  def new_comment
    UserMailer.with(user: User.last).new_comment(Comment.last.user, Comment.last.project)
  end

  def project_evaluated
    UserMailer.with(user: User.last).project_evaluated(Project.last.account.executive, Project.last)
  end


  
end
