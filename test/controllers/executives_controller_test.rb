require 'test_helper'

class ExecutivesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get executives_index_url
    assert_response :success
  end

  test "should get show" do
    get executives_show_url
    assert_response :success
  end

  test "should get new" do
    get executives_new_url
    assert_response :success
  end

  test "should get edit" do
    get executives_edit_url
    assert_response :success
  end

end
