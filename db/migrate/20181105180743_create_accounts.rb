class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :name
      t.references :client, foreign_key: {to_table: :users}
      t.references :executive, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
