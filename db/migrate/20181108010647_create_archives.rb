class CreateArchives < ActiveRecord::Migration[5.2]
  def change
    create_table :archives do |t|
      t.string :title
      t.string :description
      t.boolean :approved, default: false
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
