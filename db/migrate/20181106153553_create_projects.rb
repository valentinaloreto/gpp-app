class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.references :account, foreign_key: true
      t.string :name
      t.string :description
      t.string :responsible
      # t.boolean :is_grilla, default: false
      t.integer :kind, default: 0
      t.integer :status, default: 0
      t.date :start_date
      t.date :deadline
      t.string :next_steps
      t.references :tag, foreign_key: true

      t.timestamps
    end
  end
end
