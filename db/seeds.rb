# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Role.create({kind:"admin"})
Role.create({kind:"executive"})
Role.create({kind:"client"})

@first_user = User.create({email:"email@email.com", password:"123456"})

@first_user.build_profile({
  firstname: "Admin",
  lastname: "",
  role_id: "1"
}).save
